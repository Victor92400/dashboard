import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Route from './route';
import './App.css';
import Dots from './Widgets/WidgetPro/Dots';
import Pie from './Widgets/WidgetPro/Pie';
import Column from './Widgets/WidgetPro/Column';
import Market from './Widgets/WidgetPro/Market';
import Graph from './Widgets/WidgetPro/Graph';
import Line from './Widgets/WidgetPro/Line';
import './button.css';
import {
  ResponsiveContainer
} from 'recharts';

import { Container, Row, Col } from 'reactstrap';

const style = {
  display: 'inline',
  backgroundColor: 'lightgrey',
  width: '33.333%'
};
const stylebody = {
  backgroundColor: "lightgrey"
}
const styleDash = {
  fontFamily: 'Montserrat',
  fontSize: '130%'
}
const styleformulaire = {
  marginTop: '2%'
}

class App extends Component {

  state = {
    dashboard: true
  }

  switchpage = () => {
    const doesShow = this.state.dashboard;
    this.setState({ dashboard: !doesShow })
  }

  render() {

    let showdashboard = null;
    if (this.state.dashboard) {
      showdashboard = (
        <div>
          <ResponsiveContainer height="100%" width="100%">
            <Container fluid style={stylebody}>
              <Row>
                <Col xs="12" sm="6" lg="4">
                  <p style={style}>
                    <Dots />
                  </p>
                </Col>
                <Col xs="12" sm="6" lg="4">
                  <p style={style}>
                    <Column />
                  </p>
                </Col>
                <Col xs="12" sm="6" lg="4">
                  <p style={style}>
                    <Line />
                  </p>
                </Col> <br />
                <Col xs="12" sm="6" lg="5">
                  <p style={style}>
                    <Graph />
                  </p>
                </Col>
                <Col xs="12" sm="6" lg="3">
                  <p style={style}>
                    <Pie />
                  </p>
                </Col>
                <Col xs="12" sm="6" lg="4">
                  <p style={style}>
                    <Market />
                  </p>
                </Col>
              </Row>
              <br />
              <center><p style={styleDash}><Link style={{ color: '#8F77E6' }} to='/formulaire' onClick={this.switchpage}>Send Data</Link></p></center>
              <br />
              <Route />
            </Container>
          </ResponsiveContainer>
        </div>
      )
    }
    else {
      showdashboard = (
        <div style={styleformulaire}>
          <center><form>
            <label>
              <input type="text" name="name" placeholder="Any type of data" />
            </label> <br />
            <button className="button"><span>Send </span></button>
          </form>
          </center>
          <br />
          <center><p style={styleDash}><Link style={{ color: '#8F77E6' }} to='/' onClick={this.switchpage}>Go back to Dashboard</Link></p></center>
        </div>
      )
    }
    return (
      <div>
        {showdashboard}
      </div>
    );
  }
}
export default App;