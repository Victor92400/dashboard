import React, { Component } from 'react';
import Example from './WidgetPro/Dots';
import Pie from './WidgetPro/Pie';
import Column from './WidgetPro/Column';
import Market from './WidgetPro/Market';
import Graph from './WidgetPro/Graph';
import Line from './WidgetPro/Line';

class Widgets extends Component {
    render() {
        return (
            <div>
                <Example />
                <Pie />
                <Column />
                <Market />
                <Graph />
                <Line />
            </div>
        );
    }
}
export default Widgets;